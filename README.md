# Templates

Simple Vorlagen zum Basteln.<br />
Simple templates for handicrafts.

<h1>EN:</h1>
<h2>Sundial</h2>
<ul>
    <li>simple flat (horizontal) sundial for printing</li>
    <li>arrow aims in southern direction</li>
    <ul>
        <li>has to be adapted accoring local position</li>
        <li>has to be further adapted according current north magnetic pole</li>
    </ul>
</ul>


<h1>DE:</h1>
<h2>Sonnenuhr</h2>
<ul>
    <li>einfache horizontale Sonnenuhr zum Ausdrucken</li>
    <li>Pfeil zeigt in südliche Richtung</li>
    <ul>
        <li>muss entsprechend aktueller Position angepasst werden</li>
        <li>muss ebenso entsprechend aktueller Lage des magnetischen Nordpols angepasst werden</li>
    </ul>
</ul>

<br /><br /><br />
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>
<br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International Lizenz</a>.
<br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.